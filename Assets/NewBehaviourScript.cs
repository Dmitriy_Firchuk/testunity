﻿using UnityEngine;
using System.Collections;

public class NewBehaviourScript : MonoBehaviour {

	public float speed;
	public float turnSpeed;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.UpArrow))
			transform.Translate (Vector3.forward * speed * Time.deltaTime);
		
		if (Input.GetKey (KeyCode.DownArrow))
			transform.Translate (-Vector3.forward * speed * Time.deltaTime);
		
		if (Input.GetKey (KeyCode.LeftArrow))
			transform.Rotate (Vector3.up, -turnSpeed * Time.deltaTime);
		
		if (Input.GetKey (KeyCode.RightArrow))
			transform.Rotate (Vector3.up, turnSpeed * Time.deltaTime);
		
	}
}
